# Speedweve

This is a speedweve-like loom used to darn clothes. It uses lasercut 3mm plywood and PLA 3D-printed hooks. 

## License
This work is licensed under CC4R: <https://constantvzw.org/wefts/cc4r.en.html>.